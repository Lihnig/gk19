package vorname;

import java.awt.EventQueue;
import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Klausur extends JFrame {

	private JPanel contentPane;
	private Turtle t;

	public Klausur() {
		super("Klausur");
		createGUI();
	}
	
	private void createGUI() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 905, 698);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnAufg1a = new JButton("Aufgabe 1a");
		btnAufg1a.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aufg1a();
			}
		});
		btnAufg1a.setBounds(12, 12, 435, 25);
		contentPane.add(btnAufg1a);
		
		JButton btnAufg1b = new JButton("Aufgabe 1b");
		btnAufg1b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aufg1b();
			}
		});
		btnAufg1b.setBounds(456, 12, 435, 25);
		contentPane.add(btnAufg1b);
		
		JButton btnAufg2a = new JButton("Aufgabe 2a");
		btnAufg2a.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aufg2a();
			}
		});
		btnAufg2a.setBounds(12, 635, 435, 25);
		contentPane.add(btnAufg2a);
		
		JButton btnAufg2b = new JButton("Aufgabe 2b");
		btnAufg2b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aufg2b();
			}
		});
		btnAufg2b.setBounds(456, 635, 435, 25);
		contentPane.add(btnAufg2b);
		
		t = new Turtle(350, 300);
		t.setBounds(12, 48, 769, 572);
		contentPane.add(t);
		
		JButton btnLoeschen = new JButton("löschen");
		btnLoeschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				t.loeschen();
				t.neuStart(350, 300);
			}
		});
		btnLoeschen.setBounds(793, 49, 98, 572);
		contentPane.add(btnLoeschen);
	
	}

	private void aufg1a() {   // Aufgabe 1a
		int zaehler =0;		
		while (zaehler <10){
			t.vor(50);
			t.drehen(36);
			zaehler ++;
		}
		
	}
	
	public void quadrat(double breite){   // muss double sein, da Fließkommazahlen als Seitenlänge erlaubt
		int i = 0;
		while (i < 4) {
			t.vor(breite);
			t.drehen(-90);
			i++;							//Zählvariable muss zählen (+1)
			}
	}
	
	private void aufg1b() {   // Aufgabe 1b
		double b = 4.5;
		int zaehler =0;  				//Zählvariable muss deklariert und initialisiert werden
		while (zaehler < 22) {
			quadrat(b);
			b = b + b * 1 / 4;
			zaehler++;
			
		}
	}
	
	private void aufg2a() {   // Aufgabe 2a
	String Wert=JOptionPane.showInputDialog("Minuten?");
	int Minuten=Integer.parseInt(Wert);
	JOptionPane.showMessageDialog(this,"Flat: 20€, Basic: " + (Minuten*0.09+5) +"€");
		
	}
	

		
	

	private void aufg2b() {   // Aufgabe 2b
		int flat=20;
		double basic;
		String Wert=JOptionPane.showInputDialog("Minuten?");
		int Minuten=Integer.parseInt(Wert);
		basic=Minuten*0.09+5;
		if (basic >= flat){
			JOptionPane.showMessageDialog(this,"Flat!");
		}
		else{
			JOptionPane.showMessageDialog(this,"Basic!");
		}
		
	}
	
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Klausur frame = new Klausur();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
